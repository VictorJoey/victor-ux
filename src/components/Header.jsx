import React from 'react'

function Header() {
  return (
    <div>

<div
            data-collapse="medium"
            data-animation="default"
            data-duration="400"
            data-easing="ease"
            data-easing2="ease"
            role="banner"
            className="navigation w-nav">
            <div className="navigation-wrap">
                <a href="/" aria-current="page" className="logo-link w-nav-brand w--current">
                <img src="https://victorjoey.github.io/victor-ux/assets/img/profile.png" width="70" alt="" className="logo-image" />
                <h5 className="heading-22"><strong className="bold-text">Victor Joey</strong></h5></a>
                <div className="menu">
                <nav role="navigation" className="navigation-items w-nav-menu">
                    <a href="/" className="navigation-item w-nav-link w--current">Home</a>
                    <a href="/about" className="navigation-item w-nav-link ">About</a>
                    <a href="/resume" className="navigation-item w-nav-link ">Resume</a>
                    { /* <a href="/contact" className="navigation-item w-nav-link ">Contact</a> */ }
                </nav>
                <div className="menu-button w-nav-button">
                    <img
                    src="https://assets-global.website-files.com/6589829c7efa8f55638314fd/6589829e7efa8f55638315ff_menu-icon.png"
                    width="22"
                    alt=""
                    className="menu-icon"
                    />
                </div>
                </div>
            </div>
            </div>

    </div>
  )
}

export default Header