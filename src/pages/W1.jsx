import React from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'


function W1() {
  return (
    <div>
      <Header />
      <section className="blog-main-section">
  <div className="page-padding">
    <div className="blog-top-conttt">
      <img
        alt=""
        loading="lazy"
        width={49}
        src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659221a0ae2f094a61f3ac1f_658f83e8a47d079aa35f7c8a_Group%2520806.png"
        className="blog-tag"
      />
      <h1 className="blog-main-heading">
        Orka Pay - Empowering shift workers with Enhanced Financial Visibility
        and Predictability
      </h1>
      <p className="paragraph-45">
        Leading Orka Pay's growth from a simple wage access service to an
        advanced financial empowerment tool, I collaborated closely with a
        diverse team including a project manager and 3 engineers. Together, we
        reshaped the platform through a carefully crafted four-phase strategy,
        focusing on what users needed. This approach led to a doubling of user
        retention, a significant jump in new sign-ups, and recognition in our
        industry, all while boosting the team spirit and engagement of both our
        users and team.
      </p>
      <div className="_3rd-role-others">
        <div id="w-node-_6f67a65a-aa19-3cfc-55c1-3114b1bc340f-e2ed2578">
          <div className="blog-tags">
            <strong>Role</strong>
          </div>
          <div>Lead Product Designer</div>
        </div>
        <div id="w-node-_5dd48265-d238-aee1-aeb3-6c3016465b49-e2ed2578">
          <div className="blog-tags">
            <strong>Timeline</strong>
          </div>
          <div>5 months (Jan - Jun '23)</div>
        </div>
        <div id="w-node-_8073f348-51b4-8723-e645-34f2cdbeb13a-e2ed2578">
          <div className="blog-tags">
            <strong>Core Responsibilities</strong>
          </div>
          <div>
            Leading producr design, product strategy, research, interaction
            design, A/B testing, product roadmap.
          </div>
        </div>
      </div>
    </div>
  </div>
  <div className="top-img-2">
    <div id="w-node-_33b2b39f-224a-0f94-70f0-30067f6073a8-e2ed2578">
      <img
        alt=""
        loading="lazy"
        src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65933d4acd940a3b0098ede1_658ee3eb93829cd0bf9f9c88_Before.png"
      />
    </div>
    <div id="w-node-c7eb07d6-cc39-a1e7-e7f8-45664ffb4e80-e2ed2578">
      <img
        alt=""
        loading="lazy"
        src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65933d4acd940a3b0098edea_658ee3ef98cc4bb0e998141c_Final.png"
      />
    </div>
  </div>
  <div className="page-padding">
    <div className="all-cont">
      <div className="left-menu">
        <div
          data-animation="default"
          data-collapse="medium"
          data-duration={400}
          data-easing="ease"
          data-easing2="ease"
          role="banner"
          className="navbar-2 w-nav"
        >
          <div className="text-block-4 overview">Overview</div>
          <div className="container-2 w-container">
            <nav role="navigation" className="blog-menu w-nav-menu">
              <a
                href="w1#Problems"
                className="nav-link w-nav-link"
              >
                Problem
              </a>
              <a
                href="w1#goalse-sec"
                className="nav-link w-nav-link"
              >
                Goals
              </a>
              <a
                href="w1#Solution--sec"
                className="nav-link w-nav-link"
              >
                Solution
              </a>
              <a
                href="w1#Results"
                className="nav-link w-nav-link"
              >
                Results
              </a>
              <div className="wrapper dfdf" />
              <p className="paragraph-18">Additional Context</p>
              <a
                href="w1#Strategicphase1of4"
                className="nav-link kj w-nav-link"
              >
                User Research
              </a>
              <a
                href="w1#Strategicphase2of4"
                className="nav-link w-nav-link"
              >
                Xfn Collaboration
              </a>
              <a
                href="w1#validation-sec"
                className="nav-link w-nav-link"
              >
                Validation
              </a>
              <a
                href="w1#beta-lunch"
                className="nav-link w-nav-link"
              >
                Beta Launch
              </a>
              <a
                href="w1#future-steps"
                className="nav-link w-nav-link"
              >
                <strong className="bold-text-2">Future steps</strong>
              </a>
              <a
                href="w1#Reflection"
                className="nav-link w-nav-link"
              >
                Reflection
              </a>
            </nav>
          </div>
        </div>
      </div>
      <div className="rigth-contt">
        <div id="Problems" className="problems">
          <div className="w-richtext">
            <p>Problem &amp; Insights</p>
            <h4>
              <strong>
                Shift workers feel frustrated by unclear pay schedules and
                uncertain income, making it hard to plan their finances
              </strong>
            </h4>
            <p>
              Shift workers using Orka Pay faced several issues, including
              ambiguous pay cycles, unclear withdrawal processes, lack of time
              tracking, uncertain future earnings projections, and concerns
              about financial stability. Crucially, workers identified a gap in
              the system: our app required substantial improvements in user
              experience and functionality.
            </p>
          </div>
        </div>
        <div id="problem-col1-2" className="prob-col-1">
          <div>
            <img
              alt=""
              loading="lazy"
              src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/659408b3796b1fd3a3536e17_slack.png"
              sizes="(max-width: 479px) 100vw, (max-width: 767px) 32vw, (max-width: 991px) 31vw, (max-width: 1279px) 33vw, 422.6875px"
              srcSet="
              https://assets-global.website-files.com/6589829e7efa8f55638315f0/659408b3796b1fd3a3536e17_slack-p-500.png   500w,
              https://assets-global.website-files.com/6589829e7efa8f55638315f0/659408b3796b1fd3a3536e17_slack-p-800.png   800w,
              https://assets-global.website-files.com/6589829e7efa8f55638315f0/659408b3796b1fd3a3536e17_slack-p-1080.png 1080w,
              https://assets-global.website-files.com/6589829e7efa8f55638315f0/659408b3796b1fd3a3536e17_slack.png        1512w
            "
            />
            <div className="w-richtext">
              <p>Frustrated workers</p>
            </div>
          </div>
          <div>
            <img
              alt=""
              loading="lazy"
              src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f85173bf50096abc3cfff_Group%20805.png"
              sizes="(max-width: 479px) 100vw, (max-width: 767px) 32vw, (max-width: 991px) 31vw, (max-width: 1279px) 33vw, 422.703125px"
              srcSet="
              https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f85173bf50096abc3cfff_Group%20805-p-500.png   500w,
              https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f85173bf50096abc3cfff_Group%20805-p-800.png   800w,
              https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f85173bf50096abc3cfff_Group%20805-p-1080.png 1080w,
              https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f85173bf50096abc3cfff_Group%20805.png        1512w
            "
            />
            <div className="w-richtext">
              <p>Financial unstability leads to demotivation</p>
            </div>
          </div>
        </div>
        <div id="goalse-sec" className="goalse-sec">
          <div className="w-richtext">
            <p>Goals</p>
            <h4>
              <strong>
                From Confusion to Contentment, Orka Pay's Journey to Empower
                Workers
              </strong>
            </h4>
            <p>
              We had to reconsider the app's capabilities to make Orka Pay a
              platform that genuinely empowers workers and leaves them satisfied
              with their earnings. Challenging this, I redirected our team
              towards a concentrated emphasis on the addition of new features.
              Guided by user feedback and an awareness of workers' confusion
              stemming from an incomplete feature set, I formulated a
              comprehensive four-phase strategy. This goal was to enhance the
              platform by improving transparency in pay cycles, simplifying the
              withdrawal process, implementing time tracking and earnings
              forecasts, and offering tools to facilitate better financial
              planning and stability.
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f858b22d0bb27fee88a4d_Final-2.png"
                  alt=""
                />
              </div>
            </figure>
            <p>
              <em>
                Rather than committing to a large lift, it was best to approach
                a new feature with only the most important needs of Orka Pay’s
                target users. We could then move forward with more ambitious
                feature sets depending on success/failure.
              </em>
            </p>
          </div>
        </div>
        <div id="Solution--sec" className="solution--sec">
          <div className="w-richtext">
            <p>Solution</p>
            <h4>
              <strong>
                Introducing new features with a steadfast commitment to a
                user-centric approach, enhancing workers' financial security
                while providing improved clarity and foresight.
              </strong>
            </h4>
            <p>
              Through collaboration with our product manager, developers, QA
              testers, data analysts, and customer support team, we transformed
              the features of Orka Pay from a challenging experience into a
              user-centric solution, resulting in increased worker satisfaction,
              enhanced financial security, and improved clarity and foresight.
              My commitment to a user-centric ethos has been reinforced as I've
              grasped the paramount significance of prioritizing user needs
              within these features. This realization has deepened my
              understanding of our diverse user base and their unique
              demographics, emphasizing the profound impact it can have on the
              success of these enhanced features. This perspective has prevailed
              over the allure of trendy approaches that may appear appealing but
              might not align with our objectives. Sticking with a user-centered
              approach, with solid research and user feedback behind it, can
              make a big difference.
            </p>
            <p>
              <strong>
                Shifting from Orka Pay being a challenging experience to a
                user-centric solution
              </strong>
            </p>
            <p>‍</p>
            <p>
              <strong>Before</strong>
            </p>
            <p>Limited features</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f87c739b8ba9d6115cdcf_Before.png"
                  alt=""
                />
              </div>
            </figure>
            <p>‍</p>
            <p>
              <strong>After</strong>
            </p>
            <p>
              Ability to review past meetings right away, see relevant
              suggestions on how to improve, and prepare with ease
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f87e760c313eea31a0df6_A.png"
                  alt=""
                />
              </div>
            </figure>
            <p>‍</p>
            <p>
              <strong>After</strong>
            </p>
            <p>
              Simplifying the user experience by replacing complex infographics
              with a more straightforward and comprehensible format following
              thorough research.
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f88107ab093299dbeae04_B.png"
                  alt=""
                />
              </div>
            </figure>
            <p>‍</p>
            <p>‍</p>
            <p>
              <strong>
                Preventing users from feeling overwhelmed by too much
                information
              </strong>
            </p>
            <p>Less is more, keeping a user-centric approach</p>
            <p>‍</p>
            <p>
              <strong>Before</strong>
            </p>
            <p>
              A cluttered interface that displays current, expected, and pay
              cycles simultaneously, bombarding the user with an overload of
              information all at once.
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f86e4e7844e205d5a51b4_Final-1.png"
                  alt=""
                />
              </div>
            </figure>
            <p>
              <strong>After</strong>
            </p>
            <p>
              By implementing a progressive reveal design approach, we unveil
              essential information step by step, ensuring users receive all the
              necessary details without feeling overwhelmed.
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f891922d0bb27feea77db_Final.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div id="Results" className="resultss-sec">
          <div className="w-richtext">
            <p>Results</p>
            <h4>
              <strong>
                Culminated in an impressive 100% surge in user retention and a
                substantial 60% increase in new sign-ups, further establishing
                our position as a notable presence in the financial services
                industry.
              </strong>
            </h4>
            <p>
              User feedback vividly illustrates the transformative impact of
              Orka Pay's enhancements. Many users enthusiastically praise the
              revamped experience for its seamless user journey, while others
              enthusiastically highlight the platform's improved clarity,
              transparency, and newfound financial planning tools, underscoring
              the tangible advantages they've enjoyed. A recurring sentiment
              resonates with the newfound financial security and control,
              equipping them to forecast earnings, monitor income trends, and
              strategically plan for the future.
            </p>
            <p>‍</p>
            <figure className="w-richtext-figure-type-image w-richtext-align-center">
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f89d439b8ba9d6116b63d_Screenshot%202023-12-30%20at%2003.08.57.png"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </div>
        <div className="wrapper" />
        <div id="Strategicphase1of4" className="strategicphase1of4">
          <div className="w-richtext">
            <p>Strategic phase 1 of 4</p>
            <h4>
              <strong>
                User interviews to gain insights into pain points and usability
                challenges faced by workers, for improvements and optimizations.
              </strong>
            </h4>
            <p>
              As a leader, I worked closely with the Product Manager and our
              engineering team to carefully assess the persistent issues causing
              worker frustration. This included looking at why some workers were
              leaving their jobs and why others were expressing dissatisfaction.
              We engaged in discussions to brainstorm and implement strategies
              aimed at retaining these workers and ensuring they are happier
              with our services.
            </p>
            <p>‍</p>
            <p>
              <strong>Online Interviews</strong>
            </p>
            <p>
              <strong>‍</strong>We opted for online interviews as our research
              methodology for several compelling reasons. First and foremost, we
              wanted to eliminate any financial or time burden on our valuable
              shift workers, ensuring that their participation was both
              cost-effective and convenient. Additionally, by conducting
              interviews online, we aimed to create a comfortable and open
              environment where our shift workers felt at ease sharing their
              concerns and challenges more candidly.
            </p>
            <figure className="w-richtext-figure-type-image w-richtext-align-center">
              <div>
                <img
                  src="assets/img/interview-1.png"
                  alt=""
                />
              </div>
            </figure>
            <p>
              ‍
              <strong>
                <br />
                As an outcome
              </strong>{" "}
              of our in-depth user interviews, we identified the following pain
              points experienced by shift workers:
            </p>
            <p>
              <strong>Ambiguous Pay Cycles</strong>: Users expressed confusion
              regarding the timing and consistency of their pay cycles, leading
              to uncertainty about when they could expect their earnings.
            </p>
            <p>
              <strong>Unclear Withdrawal CTA</strong>: Users reported that they
              always struggle to find out how to withdraw earnings.
            </p>
            <p>
              <strong>Lack of Time Tracking</strong>: Many shift workers
              mentioned the absence of a time tracking feature, making it
              challenging to monitor and manage their working hours effectively.
            </p>
            <p>
              <strong>Uncertain Future Projections</strong>: Users were
              uncertain about their future earnings and lacked the tools or
              information to project their income accurately, resulting in
              financial planning difficulties.
            </p>
            <p>
              <strong>Financial Stability Concerns</strong>: The interviews
              revealed concerns about maintaining financial stability, with
              users feeling that the platform's current features did not
              adequately support their financial well-being.
            </p>
            <p>‍</p>
          </div>
        </div>
        <div id="Strategicphase2of4" className="strategicphase1of4 _22">
          <div className="w-richtext">
            <p>Strategic phase 2 of 4</p>
            <p>
              <strong>
                I conducted a few brainstorming sessions, and we came up with a
                list of features and I then created a comprehensive product
                roadmap using the MoSCoW method. these features were sorted into
                four categories, including Must Have (P1), Nice to Have (P2),
                Surprising and Delightful (P2), and Can Come Later Features(P4).
              </strong>
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f858b22d0bb27fee88a4d_Final-2.png"
                  alt=""
                />
              </div>
            </figure>
            <p>
              They were sorted in the Xfn Collaboration session
              Value-Impact-Effort (VIE) framework based on how well they could
              help achieve business goals user needs and engineering
              efforts.Product Roadmap not only infused the project goals into
              our product but also ensured we prioritized the most important
              features in the development cycle.
            </p>
            <p>‍</p>
            <h4>
              <strong>
                Balancing the presentation of earnings statistics for a diverse
                user base, including middle-aged shift workers, while addressing
                concerns about accessibility and cognitive load.
              </strong>
            </h4>
            <p>
              During one of our collaborative sessions for P1, our Product
              Manager suggested incorporating graphical representations of
              earnings statistics to illustrate users' progress. This idea
              generated a high level of excitement within the team. However, I
              raised concerns about the potential compatibility issues with our
              diverse target market, particularly since around 50% of our shift
              workers fall within a middle-aged demographic.
            </p>
            <p>
              My primary concerns revolved around accessibility and cognitive
              load. I stressed that, based on our user base's diversity, a
              text-based approach in the user interface typically offers better
              compatibility. Additionally, I highlighted that infographics can
              be visually demanding, possibly requiring users to process
              multiple elements simultaneously. This could lead to increased
              cognitive load, particularly for users with cognitive impairments
              or those unaccustomed to interpreting infographics.
            </p>
            <p>
              Despite the initial enthusiasm for the infographic approach, I
              remained dedicated to prioritizing user-centric design principles.
              Following extensive deliberation and productive discussions with
              the team, I effectively conveyed the significance of adopting a
              balanced approach. Consequently, we arrived at a consensus to
              create two versions of the feature and proceed with an A/B testing
              strategy. This strategic decision will enable us to collect
              invaluable user feedback and data, ensuring that the feature
              caters to the preferences and comprehension levels of all our
              users, regardless of their age or background.
            </p>
            <p>‍</p>
            <h4>
              <strong>
                Transitioning from an unclear and incomplete feature set to a
                precise and comprehensive one.
              </strong>
            </h4>
            <p>Introducing new features</p>
            <p>
              <strong>Payday Clarity</strong>: Providing a prominent display of
              upcoming payday dates for improved visibility and planning.
            </p>
            <p>
              <strong>Streamlined Navigation</strong>: Implementing a clear and
              distinct separation between account management functions and the
              withdrawal process for enhanced user navigation.
            </p>
            <p>
              <strong>Pay Cycle Insights</strong>: Presenting pay cycles in a
              user-friendly infographic format, allowing users to easily
              visualize their current and past earnings periods.
            </p>
            <p>
              <strong>Earnings Forecast</strong>: Offering a feature that
              forecasts future earnings, empowering users with proactive
              financial planning.
            </p>
            <p>
              <strong>Earnings Trends</strong>: Highlighting trends in earnings
              by showcasing the highest and lowest earning periods, providing
              valuable insights into income patterns.
            </p>
            <p>‍</p>
            <p>Before</p>
            <p>Unclear withdrawal option</p>
            <p>No current earning information</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f87c739b8ba9d6115cdcf_Before.png"
                  alt=""
                />
              </div>
            </figure>
            <p>After</p>
            <p>
              Visibility on upcoming salary dates, transparent earning
              information, the ability to compare past and present income,
              forecasts of future earnings, and the convenience of easy
              withdrawals.
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f87e760c313eea31a0df6_A.png"
                  alt=""
                />
              </div>
            </figure>
            <p>After</p>
            <p>
              Visibility on upcoming salary dates, transparent earning
              information, the ability to compare past and present income,
              forecasts of future earnings, and the convenience of easy
              withdrawals.
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f88107ab093299dbeae04_B.png"
                  alt=""
                />
              </div>
              <figcaption>
                <br />
                <br />‍
              </figcaption>
            </figure>
          </div>
        </div>
        <article id="validation-sec" className="validation-sec">
          <div className="w-richtext">
            <p>Strategic phase 3 of 4</p>
            <p>
              <strong>Validation</strong>
            </p>
            <h4>
              <strong>
                User preferences in A/B testing favored infographic-free
                features, validating our commitment to user-friendly design and
                satisfaction
              </strong>
            </h4>
            <p>
              Throughout our A/B testing process, we conducted multiple rounds
              to assess two versions of our features—one with infographics and
              the other without. Notably, a significant portion of our users,
              including middle-aged shift workers, consistently preferred the
              version without infographics, finding it more straightforward to
              understand. This outcome was particularly significant considering
              the demographic diversity within our user base. Furthermore, users
              expressed satisfaction with the additional features we introduced,
              such as payday clarity, streamlined navigation, pay cycle
              insights, earnings forecasts, and earnings trends. They also
              appreciated the placement of the withdrawal option near the
              balance card, which they found more intuitive and convenient.
              Additionally, users liked the contrast on the second screen,
              emphasizing our commitment to enhancing the user experience based
              on valuable feedback and user preferences.
            </p>
            <h3>‍</h3>
            <p>‍</p>
          </div>
        </article>
        <article id="beta-lunch" className="validation-sec">
          <div className="w-richtext">
            <p>Strategic phase 4 of 4</p>
            <h4>
              <strong>Beta Launch: Enhancing User Experience</strong>
            </h4>
            <p>
              During the beta launch of our project, we leveraged the valuable
              insights gathered from the soft launch to further refine the user
              experience of Orka Pay. The primary objective was to collect
              crucial user feedback on the revamped platform.
            </p>
            <p>
              Users overwhelmingly praised the new features, including Payday
              Clarity, Earnings Forecast, and Earnings Trends. These features
              provided transparency and instilled a sense of hope for financial
              stability among our user base.
            </p>
            <p>
              However, it became apparent that some users found the presence of
              separate tabs for account management and withdrawals to be an
              unnecessary complexity. In response to this feedback, I took
              proactive measures to enhance the user interface. The redesign
              involved the removal of tabs and the addition of a prominent
              withdrawal button located below the balance card. This streamlined
              user navigation and addressed the feedback directly, prioritizing
              user preferences and feedback review as integral components of our
              user-centric approach.
            </p>
            <p>Before</p>
            <p>Tabs to navigate between account and withdrawl</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f8c5fdc2450d9faa7d66a_B.png"
                  alt=""
                />
              </div>
            </figure>
            <p>After</p>
            <p>
              Removal of tabs and the addition of a prominent withdrawal button
              located below the balance card
            </p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/658f8c67b047557f619eeb13_A.png"
                  alt=""
                />
              </div>
            </figure>
            <p>
              Through this iterative process, we not only improved the Orka Pay
              platform but also strengthened our commitment to delivering a
              product that truly caters to the needs and expectations of our
              users.
            </p>
            <p>
              ‍<br />
              <br />‍
            </p>
          </div>
        </article>
        <article id="future-steps" className="validation-sec">
          <div className="w-richtext">
            <p>Future steps</p>
            <p>
              In our ongoing commitment to elevating the Orka Pay experience,
              we're excited to unveil a roadmap brimming with innovative product
              design features. These include a Shift Recommendation Feature to
              empower users with income-boosting opportunities, Income Stability
              Tools for goal-oriented financial planning, Budgeting Assistance
              to streamline financial management, and enriched Financial
              Education resources. To top it off, our User Feedback Loop ensures
              these enhancements remain perfectly in sync with our users'
              evolving needs and preferences. As we embark on this journey, our
              focus remains steadfast: crafting a financial empowerment tool
              that champions your financial well-being.
              <br />
              <br />
              Also by implementing these future steps, we can further support
              our users in achieving steady income and financial stability while
              reinforcing our commitment to their well-being.
            </p>
            <p>
              <strong>Shift Recommendation Feature</strong>: Implement a feature
              that suggests additional shifts or opportunities to users through
              the pay app if they are falling behind on their income or budget.
              This proactive approach can help users take control of their
              financial stability and plan their work schedule more effectively.
            </p>
            <p>
              <strong>Income Stability Tools</strong>: Develop tools within the
              app that allow users to set income goals and receive insights on
              how to achieve them. This can include guidance on adjusting their
              work hours or seeking additional income sources.
            </p>
            <p>
              <strong>Budgeting Assistance</strong>: Integrate budgeting tools
              and resources into the app to help users manage their finances
              more efficiently. This can include expense tracking, budget
              creation, and financial planning features.
            </p>
            <p>
              <strong>Financial Education</strong>: Offer resources and
              educational content within the app to enhance users' financial
              literacy. This can empower them to make informed financial
              decisions and improve their overall financial well-being.
            </p>
            <p>
              <strong>User Feedback Loop</strong>: Continuously gather user
              feedback to refine and enhance these features, ensuring they align
              with users' needs and preferences.
            </p>
          </div>
        </article>
        <article id="Reflection" className="validation-sec">
          <div className="w-richtext">
            <p>Reflection</p>
            <h4>
              <strong>
                Becoming a more influential and user-centric leader, advocating
                for what's best for users, and validating these choices.
              </strong>
            </h4>
            <p>
              Reflecting on this project, I've undergone a significant
              transformation as a leader who champions user-centric principles
              and advocates for the best interests of our users. This journey
              has reinforced the value of actively seeking user feedback and
              making data-driven decisions to enhance their experience.
            </p>
            <p>
              I've learned to be more influential in steering the project
              towards a more user-friendly and inclusive direction. This shift
              has not only improved our product but has also fostered a more
              empathetic and collaborative team dynamic.
            </p>
            <p>
              One of the key takeaways is the importance of challenging the
              status quo when necessary. Advocating for accessibility, clarity,
              and user-centric design, even in the face of initial resistance,
              has been a gratifying experience. It has solidified my commitment
              to always stand up for what is right for our users and ensure that
              our product genuinely serves their needs.
            </p>
            <p>
              As I reflect on this journey, I'm inspired to continue championing
              user-centricity and advocating for positive changes that benefit
              our users. These lessons will guide my approach in my current role
              and any future endeavors, as I strive to create products that
              truly make a difference in the lives of those who use them.
            </p>
            <p>‍</p>
            <figure
              className="w-richtext-figure-type-image w-richtext-align-fullwidth"
              style={{ maxWidth: "1514pxpx" }}
            >
              <div>
                <img
                  src="https://assets-global.website-files.com/6589829e7efa8f55638315f0/65940a51392bf0e7b9a7a4e8_Reflectionorka.png"
                  loading="lazy"
                  alt=""
                />
              </div>
            </figure>
          </div>
        </article>
      </div>
    </div>
  </div>
</section>


    <Footer />

    </div>
  )
}

export default W1